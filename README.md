# **godot** pipeline

<!-- BADGIE TIME -->

[![brettops pipeline](https://img.shields.io/badge/brettops-pipeline-209cdf?labelColor=162d50)](https://brettops.io)
[![pipeline status](https://img.shields.io/gitlab/pipeline-status/brettops/pipelines/godot?branch=main)](https://gitlab.com/brettops/pipelines/godot/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

---

**Note**: This pipeline is experimental and its API may break without warning.

---

## Usage

Include all jobs in your pipeline:

```yaml
include:
  - project: brettops/pipelines/godot
    file:
      - godot-3-gdtoolkit-gdformat.yml
      - godot-3-gdtoolkit-gdlint.yml
      - godot-3-export.yml
      - godot-4-gdtoolkit-gdformat.yml
      - godot-4-gdtoolkit-gdlint.yml
      - godot-4-export-release.yml
```

Godot 3 and 4 are supported. This pipeline has been tested with Web export but
other export presets are on the way.

Include all Godot 3 jobs:

```yaml
include:
  - project: brettops/pipelines/godot
    file:
      - godot-3-gdtoolkit-gdformat.yml
      - godot-3-gdtoolkit-gdlint.yml
      - godot-3-export.yml
```

Include all Godot 4 jobs:

```yaml
include:
  - project: brettops/pipelines/godot
    file:
      - godot-4-gdtoolkit-gdformat.yml
      - godot-4-gdtoolkit-gdlint.yml
      - godot-4-export-release.yml
```

## Jobs

### godot-3-gdformat.yml

Run `gdformat` on Godot 3 GDScript code.

### godot-3-gdlint.yml

Run `gdlint` on Godot 3 GDScript code.

### godot-3-export.yml

Run `godot --export` on a Godot 3 project.

### godot-4-gdformat.yml

Run `gdformat` on Godot 4 GDScript code.

### godot-4-gdlint.yml

Run `gdlint` on Godot 4 GDScript code.

### godot-4-export-release.yml

Run `godot --export-release` on a Godot 4 project.

## Variables

### GODOT_EXPORT_PRESET

The export preset to use. Must be defined in the project.godot file. Defaults to
`Web`.

### GODOT_EXPORT_PATH

The export path to use. Defaults to
`${CI_PROJECT_DIR}/dist/${GODOT_EXPORT_PRESET}/index.html` for compatibility
with the Web preset.

### GODOT_EXPORT_OPTS

Extra arguments to pass to the `godot` CLI during export.
